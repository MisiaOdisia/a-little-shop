package com.app.littleshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LittleShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(LittleShopApplication.class, args);
	}

}
