package com.app.littleshop.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.littleshop.error.UserAlreadyExistException;
import com.app.littleshop.model.data.request.RegisterUserRequest;
import com.app.littleshop.service.UserService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("api/user/registration")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class RegistrationController {
	
	private UserService userService;
	
	@PostMapping
	public void registerUserAccount(@Valid @RequestBody RegisterUserRequest userRequest) throws UserAlreadyExistException {
		userService.registerUserAccount(userRequest);
	}

}
