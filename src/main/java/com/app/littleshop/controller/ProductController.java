package com.app.littleshop.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.littleshop.model.data.request.ProductRequest;
import com.app.littleshop.model.data.response.ProductDto;
import com.app.littleshop.service.ProductService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("api/products")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ProductController {
	
	private ProductService productService;	
	
	@GetMapping
	public List<ProductDto> getAll(
			@RequestParam(defaultValue = "") String category,
			@RequestParam(defaultValue = "0") Integer pageNumber, 
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy,
            @RequestParam(defaultValue = "asc") String sortOrder) {
		
		if(category.isBlank())		
			return productService.getAll(pageNumber, pageSize, sortBy, sortOrder);
		else 
			return productService.getAllByCategory(category, pageNumber, pageSize, sortBy, sortOrder);
			
	}
	
	@GetMapping("{id}")
	public ResponseEntity<ProductDto> getById(@PathVariable(value = "id") long id){
		return productService.getByIdOrNull(id)
				.map(ResponseEntity::ok)
				.orElse(ResponseEntity.notFound().build());
	}
	
	@PostMapping
	public long create(@Valid @RequestBody ProductRequest product) {
		return productService.save(product);
	}
	
	@PutMapping("{id}")
	public ResponseEntity<ProductDto> update(@PathVariable(value = "id") long id, @Valid @RequestBody ProductRequest product) {
		return productService.updateOrNull(id, product)
				.map(ResponseEntity::ok)
				.orElse(ResponseEntity.notFound().build());
	}

	@DeleteMapping("{id}")
	public ResponseEntity<Void> delete(@PathVariable(value = "id") long id) {
		return productService.deleteOrNull(id)
				.map(response -> new ResponseEntity<Void>(HttpStatus.OK))
				.orElse(ResponseEntity.notFound().build());	
	}

}
