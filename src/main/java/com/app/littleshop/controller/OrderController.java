package com.app.littleshop.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.littleshop.error.OrderNotFoundException;
import com.app.littleshop.error.UserDontExistException;
import com.app.littleshop.model.data.request.OrderRequest;
import com.app.littleshop.model.data.response.OrderDto;
import com.app.littleshop.service.OrderService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("api/orders")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class OrderController {

	private OrderService orderService;	
	
	@GetMapping
	public List<OrderDto> getAll() {	
		return orderService.getAll();
			
	}
	
	@GetMapping("{id}")
	public ResponseEntity<OrderDto> getById(@PathVariable(value = "id") Long id){
		return orderService.getById(id)
				.map(ResponseEntity::ok)
				.orElse(ResponseEntity.notFound().build());
	}
	
//	@GetMapping
//	public List<OrderDto> getAllByUserId(@RequestParam(name = "userId")  Long userId) throws UserDontExistException {
//		return orderService.getAllByUserId(userId);
//	}
	
	@PostMapping
	public long create(@Valid @RequestBody OrderRequest order) throws UserDontExistException {
		return orderService.save(order);
	}
	
	@PutMapping("{id}")
	public OrderDto update(@PathVariable(value = "id") Long id, @Valid @RequestBody OrderRequest order) throws OrderNotFoundException, UserDontExistException {
		return orderService.update(id, order);
	}

	@DeleteMapping("{id}")
	public ResponseEntity<Void> delete(@PathVariable(value = "id") Long id) {
		return orderService.delete(id)
				.map(response -> new ResponseEntity<Void>(HttpStatus.OK))
				.orElse(ResponseEntity.notFound().build());	
	}
}
