package com.app.littleshop.enums;

public enum Category {
	TOP,
	SWEATER,
	DRESS,
	SKIRT,
	TROUSERS,
	COAT, 
	JACKET
}
