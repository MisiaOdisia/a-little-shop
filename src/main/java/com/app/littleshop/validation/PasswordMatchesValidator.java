package com.app.littleshop.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.app.littleshop.model.data.request.RegisterUserRequest;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> { 
   
  @Override
  public void initialize(PasswordMatches constraintAnnotation) { }
  
  @Override
  
  public boolean isValid(Object obj, ConstraintValidatorContext context){   
      RegisterUserRequest user = (RegisterUserRequest) obj;
      return user.getPassword().equals(user.getMatchingPassword());    
  }     
}
