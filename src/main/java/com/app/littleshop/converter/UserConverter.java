package com.app.littleshop.converter;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.app.littleshop.model.data.request.RegisterUserRequest;
import com.app.littleshop.model.data.response.OrderDto;
import com.app.littleshop.model.data.response.UserDto;
import com.app.littleshop.model.entity.UserEntity;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserConverter {
	
	private PasswordEncoder passwordEncoder;
	private OrderConverter orderConverter;
	
	public UserDto toDto (UserEntity entity) {
		List<OrderDto> productDtos = entity.getOrders()
				.stream()
				.map(orderConverter::toDto)
				.collect(Collectors.toList());
		
		return new UserDto(entity.getId(), entity.getFirstName(), entity.getLastName(), entity.getEmail(), productDtos);
	}
	
	public UserEntity toEntity (RegisterUserRequest request) {
		return new UserEntity(request.getFirstName(), request.getLastName(),
				passwordEncoder.encode(request.getPassword()), request.getEmail(), Collections.emptyList());
	}
}
