package com.app.littleshop.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.app.littleshop.model.data.request.OrderRequest;
import com.app.littleshop.model.data.response.OrderDto;
import com.app.littleshop.model.data.response.ProductDto;
import com.app.littleshop.model.entity.OrderEntity;
import com.app.littleshop.model.entity.ProductEntity;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class OrderConverter {
	
	private ProductConverter productConverter;
	
	public OrderDto toDto (OrderEntity entity) {
		List<ProductDto> productDtos = entity.getProducts()
				.stream()
				.map(productConverter::toDto)
				.collect(Collectors.toList());
		
		return new OrderDto(entity.getId(), entity.getOrderDate(), productDtos);
	}
	
	public OrderEntity toEntity (OrderRequest request) {
		List<ProductEntity> productsEntity = request.getProducts()
				.stream()
				.map(productConverter::toEntity)
				.collect(Collectors.toList());
		
		return new OrderEntity(request.getOrderDate(), productsEntity);
	}
}
