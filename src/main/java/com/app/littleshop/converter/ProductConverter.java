package com.app.littleshop.converter;

import org.springframework.stereotype.Component;

import com.app.littleshop.enums.Category;
import com.app.littleshop.model.data.request.ProductRequest;
import com.app.littleshop.model.data.response.ProductDto;
import com.app.littleshop.model.entity.ProductEntity;

@Component
public class ProductConverter {
	
	public ProductDto toDto (ProductEntity entity) {
		return new ProductDto(entity.getId(), entity.getName(), entity.getPrice(), entity.getCategory(), entity.getDescription());
	}
	
	public ProductEntity toEntity (ProductRequest request) {
		return new ProductEntity(request.getName(), request.getPrice(), Category.valueOf(request.getCategory().toUpperCase()), request.getDescription());
	}

}
