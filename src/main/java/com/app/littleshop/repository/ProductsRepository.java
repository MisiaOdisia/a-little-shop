package com.app.littleshop.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.app.littleshop.enums.Category;
import com.app.littleshop.model.entity.ProductEntity;

@Repository
public interface ProductsRepository extends PagingAndSortingRepository<ProductEntity, Long> {
	
	List<ProductEntity> findAllByCategory(Category category, Pageable pageable);

}
