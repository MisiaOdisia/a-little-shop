package com.app.littleshop.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.app.littleshop.model.entity.OrderEntity;
import com.app.littleshop.model.entity.UserEntity;


@Repository
public interface OrderRepository extends CrudRepository<OrderEntity, Long> {

	//public List<OrderEntity> findAllByUser_User(UserEntity user);
}
