package com.app.littleshop.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.app.littleshop.model.entity.UserEntity;
import com.app.littleshop.repository.UserRepository;

import lombok.AllArgsConstructor;

@Service
@Transactional
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class MyUserDetailsService implements UserDetailsService {
	
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		 UserEntity user = userRepository.findByEmail(email);
		 
	     if (user == null) {
	         throw new UsernameNotFoundException(
	         "No user found with username: "+ email);
	     }
	        
	     boolean enabled = true;
	     boolean accountNonExpired = true;
	     boolean credentialsNonExpired = true;
	     boolean accountNonLocked = true;
	     return  new org.springframework.security.core.userdetails.User(user.getEmail(),user.getPassword().toLowerCase(), enabled, accountNonExpired, 
	          credentialsNonExpired, accountNonLocked, null); 
	}
}
