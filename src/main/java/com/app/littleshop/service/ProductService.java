package com.app.littleshop.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.app.littleshop.converter.ProductConverter;
import com.app.littleshop.enums.Category;
import com.app.littleshop.enums.OperationStatus;
import com.app.littleshop.model.data.request.ProductRequest;
import com.app.littleshop.model.data.response.ProductDto;
import com.app.littleshop.repository.ProductsRepository;

import lombok.AllArgsConstructor;

@Service
@Transactional
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ProductService {
	
	private ProductsRepository productRepository;
	private ProductConverter converter;
	
	public List<ProductDto> getAll(Integer pageNumber, Integer pageSize, String sortBy, String sortOrder) {
		Pageable paging = createPaging(pageNumber, pageSize, sortBy, sortOrder);
		
		return StreamSupport.stream(productRepository.findAll(paging).spliterator(), false)
				.map(converter::toDto)
				.collect(Collectors.toList());
	}
	
	public List<ProductDto> getAllByCategory(String category, Integer pageNumber, Integer pageSize, String sortBy, String sortOrder) {
		Pageable paging = createPaging(pageNumber, pageSize, sortBy, sortOrder);
		
		return StreamSupport.stream(productRepository.findAllByCategory(Category.valueOf(category.toUpperCase()), paging).spliterator(), false)
				.map(converter::toDto)
				.collect(Collectors.toList());
	}
	
	public Optional<ProductDto> getByIdOrNull(long id) {
		return productRepository.findById(id)
				.map(converter::toDto);
	}
	
	public long save(ProductRequest product) {
		return productRepository.save(converter.toEntity(product)).getId();
		
	}
	
	public Optional<ProductDto> updateOrNull(long id, ProductRequest productData) {
		return productRepository.findById(id)
			.map(product -> {				
				product = converter.toEntity(productData);
				product.setId(id);
				
				return converter.toDto(productRepository.save(product));
			});
	}
	
	public Optional<OperationStatus> deleteOrNull(long id) {
		return productRepository.findById(id)
		.map(product -> {
			productRepository.delete(product);
			
			return OperationStatus.SUCCESS;
		});
	}

	private Pageable createPaging(Integer pageNumber, Integer pageSize, String sortBy, String sortOrder) {
		return PageRequest.of(pageNumber, pageSize, Sort.by(Direction.valueOf(sortOrder.toUpperCase()), sortBy));
	}
	
}
