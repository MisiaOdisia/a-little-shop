package com.app.littleshop.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.littleshop.converter.UserConverter;
import com.app.littleshop.error.UserAlreadyExistException;
import com.app.littleshop.model.data.request.RegisterUserRequest;
import com.app.littleshop.repository.UserRepository;

import lombok.AllArgsConstructor;

@Service
@Transactional
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserService {
	
	private UserRepository userRepository;
	private UserConverter converter;
	
	public void registerUserAccount(RegisterUserRequest userRequest) throws UserAlreadyExistException {
	     if (emailExist(userRequest.getEmail())) {  
	            throw new UserAlreadyExistException(
	              "There is an account with that email address: " +  userRequest.getEmail());
	      }
	     
	     userRepository.save(converter.toEntity(userRequest));	
	}
	
    private boolean emailExist(String email) {
        return userRepository.findByEmail(email) != null;
    }

}
