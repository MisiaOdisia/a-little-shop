package com.app.littleshop.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.littleshop.converter.OrderConverter;
import com.app.littleshop.enums.OperationStatus;
import com.app.littleshop.error.OrderNotFoundException;
import com.app.littleshop.error.UserDontExistException;
import com.app.littleshop.model.data.request.OrderRequest;
import com.app.littleshop.model.data.response.OrderDto;
import com.app.littleshop.model.entity.OrderEntity;
import com.app.littleshop.model.entity.UserEntity;
import com.app.littleshop.repository.OrderRepository;
import com.app.littleshop.repository.UserRepository;

import lombok.AllArgsConstructor;

@Service
@Transactional
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class OrderService {
	
	private OrderRepository orderRepository;
	private UserRepository userRepository;
	private OrderConverter converter;
	
	
	public List<OrderDto> getAll() {		
		return StreamSupport.stream(orderRepository.findAll().spliterator(), false)
				.map(converter::toDto)
				.collect(Collectors.toList());
	}
	
	public Optional<OrderDto> getById(Long id) {
		return orderRepository.findById(id)
				.map(converter::toDto);
	}
	
	public Long save(OrderRequest order) throws UserDontExistException {
		long userId = 1;
		Optional<UserEntity> userEntityOptional = userRepository.findById(userId);
		UserEntity userEntity;
		if(userEntityOptional.isPresent()) 
			userEntity = userEntityOptional.get();
		else throw new UserDontExistException("");
		
		OrderEntity orderEntity = converter.toEntity(order);
		userEntity.getOrders().add(orderEntity);
		
		userRepository.save(userEntity);		
		
		return orderEntity.getId();
		
	}
	
	public OrderDto update(Long orderId, OrderRequest orderRequest) throws OrderNotFoundException {
		Optional<OrderEntity> orderEntityOptional = orderRepository.findById(orderId);
		OrderEntity orderEntity;
		if(orderEntityOptional.isPresent())
			orderEntity = orderEntityOptional.get();
		else throw new OrderNotFoundException("");

		orderEntity = converter.toEntity(orderRequest);
		orderEntity.setId(orderId);
		
		return converter.toDto(orderRepository.save(orderEntity));
	}
	
	public Optional<OperationStatus> delete(Long id) {
		return orderRepository.findById(id)
		.map(product -> {
			orderRepository.delete(product);
			
			return OperationStatus.SUCCESS;
		});
	}
}
