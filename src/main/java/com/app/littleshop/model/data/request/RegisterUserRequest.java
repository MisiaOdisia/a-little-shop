package com.app.littleshop.model.data.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.app.littleshop.validation.PasswordMatches;
import com.app.littleshop.validation.ValidEmail;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor 
@AllArgsConstructor
@PasswordMatches
public class RegisterUserRequest {
	
    @NotNull
    @NotEmpty
    private String firstName;
     
    @NotNull
    @NotEmpty
    private String lastName;
     
    @NotNull
    @NotEmpty
    private String password;
    private String matchingPassword;
     
    @NotNull
    @NotEmpty
    @ValidEmail
    private String email;
}
