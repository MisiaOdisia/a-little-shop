package com.app.littleshop.model.data.request;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class OrderRequest {
	
	@DateTimeFormat(iso = ISO.DATE)
	private Date orderDate;
	private List<ProductRequest> products;	
}
