package com.app.littleshop.model.data.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor 
@AllArgsConstructor
public class UserDto {

	private long id;
    private String firstName;
    private String lastName;
    private String email;
    private List<OrderDto> orders;	 
}
