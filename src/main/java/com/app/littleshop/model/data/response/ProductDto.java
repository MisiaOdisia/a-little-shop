package com.app.littleshop.model.data.response;

import java.math.BigDecimal;

import com.app.littleshop.enums.Category;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor 
@AllArgsConstructor
public class ProductDto {
	
	private long id;
	private String name;
	private BigDecimal price;
	private Category category;
	private String description;
	
}
