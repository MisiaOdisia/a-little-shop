package com.app.littleshop.model.data.response;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor 
@AllArgsConstructor
public class OrderDto {
	
	private long id;
	private Date orderDate;
	private List<ProductDto> products;
}
