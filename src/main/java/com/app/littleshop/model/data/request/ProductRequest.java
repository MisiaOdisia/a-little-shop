package com.app.littleshop.model.data.request;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ProductRequest {
	
	@NotNull
	@NotEmpty
	private String name;
	@Min(0)
	private BigDecimal price;
	private String category;
	private String description;
	
}
