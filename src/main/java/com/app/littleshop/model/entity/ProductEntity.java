package com.app.littleshop.model.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.app.littleshop.enums.Category;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Getter @Setter
@RequiredArgsConstructor
@NoArgsConstructor
public class ProductEntity {

	@Id
	@Column(unique = true, nullable = false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NonNull
	private String name;
	@NonNull
	private BigDecimal price;
	@NonNull
	@Enumerated(EnumType.STRING)
	private Category category;
	@NonNull
	private String description;	
}
