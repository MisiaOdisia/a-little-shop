package com.app.littleshop.model.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Getter @Setter
@RequiredArgsConstructor
@NoArgsConstructor
public class UserEntity {
	
	@Id
	@Column(unique = true, nullable = false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NonNull
    private String firstName;
	@NonNull
    private String lastName;
	@NonNull
	@Column(length = 60)
    private String password;
	@NonNull
    private String email;
	@NonNull
	@OneToMany(cascade = CascadeType.ALL)
	private List<OrderEntity> orders;	 
}
