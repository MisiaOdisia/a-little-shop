package com.app.littleshop.error;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ControllerExceptionHandler {
	
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public List<ErrorResponse> fieldErrorExceptionHandler(MethodArgumentNotValidException exception) {
		List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
		return fieldErrors.stream()
					.map(error -> new ErrorResponse(getErrorMessage(error))															)
					.collect(Collectors.toList());
	}
	
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(IllegalArgumentException.class) 
	public ErrorResponse noEnumConstantExceptionHandler(IllegalArgumentException exception) {
		if(exception.getMessage().contains("enum"))
			return new ErrorResponse(exception.getMessage());
		else throw exception;
	}
	
	@ResponseBody
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(NoSuchElementException.class)
	public void itemNotFoundExceptionHandler(NoSuchElementException exception) { }
	
	private String getErrorMessage(FieldError fieldError) {
		return new StringBuilder()
				.append(fieldError.getField())
				.append(" ")
				.append(fieldError.getDefaultMessage())
				.toString();
	}

}
