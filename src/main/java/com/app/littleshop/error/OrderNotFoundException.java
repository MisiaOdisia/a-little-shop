package com.app.littleshop.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class OrderNotFoundException extends Exception {

	public OrderNotFoundException(String message) {
		super(message);
	}
}
