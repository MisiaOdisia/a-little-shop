package com.app.littleshop.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class UserDontExistException extends Exception {

	public UserDontExistException(String message) {
		super(message);
	}
}
